# Survival Shooter - Dani AI Edition

This project has been built using **Unity 2017.3**. It is backwards compatible with **Unity 2017.1**, 
**2017.2**, **5.6 and higher**.

The Survival Shooter is a reimplementation of Unity's Survival Tutorial using
Dani AI. All AI scripts previously within the repo:

* `EnemyMovement.cs`
* `EnemeyAttack.cs`

have been reimplemented using Dani AI as **Actions**: 

* `Attack.cs`
* `Move.cs`
* `Stop.cs`

Subsequently, **Observers** which have been implemented to supplement the **Actions** are:

* `GetPlayerPosition.cs`
* `IsAgentAlive.cs`
* `IsPlayerAlive.cs`

## Requirements
You **must** have Dani AI installed for the project to compile properly.
