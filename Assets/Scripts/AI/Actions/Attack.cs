﻿using UnityEngine;

namespace InitialPrefabs.DaniAI.Tutorial {

	/// <summary>
	/// Attempts to attack and damage the player when the player is close enough.
	/// </summary>
	[CustomMenuPath ("Tutorial/Attack")]
	public class Attack : Action {

		[Tooltip ("How far can the AI attack?")]
		public float attackRadius = 1.5f;
		[Tooltip ("How long should the AI wait before it should attack again?")]
		public float timeBetweenAttacks = 0.5f;
		[Tooltip ("How much damage should the AI do?")]
		public int damage = 10;

		[Header ("Variables")]
		[Tooltip ("What is the name of the Vector3Variable?")]
		public string vector3VariableName = "Vector3Variable";
		[Tooltip ("What is the name of the PlayerHealthVariable?")]
		public string playerHealthVariableName = "Health";

		private Vector3Variable playerPosition;
		private PlayerHealthVariable playerHealthVariable;
		private float timer;

		public override void OnStart () {
			// Get the variables
			playerPosition = Template.GetVariable<Vector3Variable> (vector3VariableName);
			playerHealthVariable = Template.GetVariable<PlayerHealthVariable> (playerHealthVariableName);

			// Reset the timer
			timer = 0f;
		}

		public override ActionState OnActionUpdate () {
			timer += Time.deltaTime;

			// Attack every 0.5 seconds and if the player is within range
			if (timer >= timeBetweenAttacks && IsWithinAttackRadius (Transform.position, playerPosition.Value)) {
				// Then succeed the task!
				AttackTarget ();
				return ActionState.Success;
			} else {
				// Otherwise, we'll fail it...
				return ActionState.Fail;
			}
		}

		public override void OnActionEnd (ActionState state) {
			switch (state) {
				// Reset the timer when the task is successful
				case ActionState.Success:
					timer = 0f;
					return;
			}
		}

		// Check if the player is within the attack radius
		private bool IsWithinAttackRadius (Vector3 start, Vector3 destination) {
			float distance = Vector3.Distance (start, destination);
			return distance <= attackRadius;
		}

		// Attack the player and damage the health
		private void AttackTarget () {
			playerHealthVariable.Value.TakeDamage (damage);
		}

	}
}