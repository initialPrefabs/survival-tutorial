﻿using UnityEngine;
using UnityEngine.AI;

namespace InitialPrefabs.DaniAI.Tutorial {

	/// <summary>
	/// Moves the agent towards a specified position.
	/// </summary>
	[CustomMenuPath ("Tutorial/Move")]
	public class Move : Action {

		[Tooltip ("How far should the AI stop before the target destination?")]
		public float stoppingDistance = 1.2f;

		[Header ("Variables")]
		[Tooltip ("What is the name of the Vector3Variable?")]
		public string vector3VariableName = "Vector3 Variable";

		private Vector3Variable position;
		private NavMeshAgent navAgent;

		public override void OnStart () {
			// Get the variable
			position = Template.GetVariable<Vector3Variable> (vector3VariableName);

			// Get a reference to the NavMeshAgent
			navAgent = GetComponent<NavMeshAgent> ();
		}

		public override void OnActionStart () {
			// Ensure that the NavMeshAgent is not stopped and set the destination
			navAgent.isStopped = false;
		}

		public override ActionState OnActionUpdate () {
			// The player is always moving - so the NavMeshAgent needs to update its position
			navAgent.SetDestination (position.Value);

			// If the NavMeshAgent is close enough to the player, succeed the Action
			if (navAgent.hasPath && navAgent.remainingDistance <= stoppingDistance) {
				return ActionState.Success;
			} else {
				// otherwise continue running the Action
				return ActionState.Running;
			}
		}

		public override void OnActionEnd (ActionState state) {
			// Stop the NavMeshAgent and clear the path as long as the NavMeshAgent 
			// is on the NavMesh
			if (navAgent.isOnNavMesh) {
				navAgent.isStopped = true;
				navAgent.ResetPath ();
			}
		}

	}
}