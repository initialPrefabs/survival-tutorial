﻿using UnityEngine;
using UnityEngine.AI;

namespace InitialPrefabs.DaniAI.Tutorial {

	/// <summary>
	/// Stops the agent from moving immediately using the NavMeshAgent.
	/// </summary>
	[CustomMenuPath ("Tutorial/Stop")]
	public class Stop : Action {

		private NavMeshAgent navAgent;

		public override void OnStart () {
			// Get the NavMeshAgent component
			navAgent = GetComponent<NavMeshAgent> ();
		}

		public override void OnActionStart () {
			// Clear the path and disable the NavMeshAgent
			navAgent.enabled = false;
		}

		public override ActionState OnActionUpdate () {
			// Immediately succeed the task immediately
			return ActionState.Success;
		}

	}
}