﻿using UnityEngine;

namespace InitialPrefabs.DaniAI.Tutorial {

	/// <summary>
	/// Constantly checks whether the player (enemy) is alive.
	/// </summary>
	[CustomMenuPath ("Tutorial/Is Player Alive")]
	public class IsPlayerAlive : BoolObserver {

		[Header ("Variables")]
		[Tooltip ("What is the name of the PlayerHealthVariable?")]
		public string healthVariableName = "Health";

		private PlayerHealthVariable healthVariable;

		public override void OnStart () {
			// Get the player gameObject and the health attached to the player
			GameObject player = GameObject.FindGameObjectWithTag ("Player");
			PlayerHealth health = player.GetComponent<PlayerHealth> ();

			// Get the HealthVariable within DaniAI 
			healthVariable = Template.GetVariable<PlayerHealthVariable> (healthVariableName);
			healthVariable.Value = health;
		}

		public override bool OnObserverUpdate () {
			return healthVariable.Value.currentHealth > 0f;
		}

	}
}