﻿using UnityEngine;

namespace InitialPrefabs.DaniAI.Tutorial {

    /// <summary>
    /// Constantly checks the agent's health and determines whether 
    /// the agent is alive.
    /// </summary>
    [CustomMenuPath ("Tutorial/Is Agent Alive")]
    public class IsAgentAlive : BoolObserver {

        private EnemyHealth enemyHealth;

        public override void OnStart () {
            enemyHealth = GetComponent<EnemyHealth> ();
        }

        public override bool OnObserverUpdate () {
            return enemyHealth.currentHealth > 0f;
        }
    }
}