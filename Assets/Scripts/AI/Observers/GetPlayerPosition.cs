﻿using UnityEngine;

namespace InitialPrefabs.DaniAI.Tutorial {

	/// <summary>
	/// Constantly observes and updates the player's position.
	/// </summary>
	[CustomMenuPath ("Tutorial/Get Player Position")]
	public class GetPlayerPosition : BoolObserver {

		[Header ("Variables")]
		[Tooltip ("What is the name of the Vector3Variable?")]
		public string vector3VariableName = "Vector3 Variable";

		private Vector3Variable playerPosition;
		private Transform player;

		// Use this for initialization
		public override void OnStart () {
			// Get a reference to the variable in DANI
			playerPosition = Template.GetVariable<Vector3Variable> (vector3VariableName);

			// Get the reference to the Player
			player = GameObject.FindGameObjectWithTag ("Player").transform;
		}

		// Runs every frame like Unity's Update() call
		public override bool OnObserverUpdate () {
			// Set the player's position
			playerPosition.Value = player.transform.position;

			// For this basic AI, we'll always return true
			return true;
		}

	}
}