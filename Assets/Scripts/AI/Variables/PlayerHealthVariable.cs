﻿using UnityEngine;

namespace InitialPrefabs.DaniAI.Tutorial {

    /// <summary>
    /// Stores a reference to the PlayerHealth inside the Template.
    /// </summary>
    public class PlayerHealthVariable : GenericVariable<PlayerHealth> { }
}