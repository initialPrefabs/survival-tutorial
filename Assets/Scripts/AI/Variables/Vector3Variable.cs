﻿using UnityEngine;

namespace InitialPrefabs.DaniAI.Tutorial {

    /// <summary>
    /// Stores the Vector3 variable inside a Template.
    /// </summary>
    public class Vector3Variable : GenericVariable<Vector3> { }
}